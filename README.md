## Introduction

**android-ingredisearch** is a sample Android application developed for tutorial purposes. The application will let the user search for recipes and favorite them.

By [following the commits](https://gitlab.com/fernandospr/android-ingredisearch/commits/master), you will see how to refactor an application to use MVP and add unit tests using Mockito.

### Notes

For this app to work, please ensure the following:

* Get your Food2Fork API key from http://food2fork.com/about/api
* Create a keystore.properties file with the following content (including the quotes):
`FOOD2FORK_API_KEY="YOUR API KEY"`
* Place this file in the root project
* This project was developed using Android Studio 3 Beta 7
