/*
 * Copyright (c) 2017 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.raywenderlich.ingredisearch.favorites

import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.raywenderlich.ingredisearch.common.Recipe
import com.raywenderlich.ingredisearch.repository.RecipeRepository
import org.junit.Before
import org.junit.Test

class FavoritesTests {

  private lateinit var repository: RecipeRepository
  private lateinit var presenter: FavoritesPresenter
  private lateinit var view: FavoritesPresenter.View

  @Before
  fun setup() {
    repository = mock()
    view = mock()
    presenter = FavoritesPresenter(repository)
    presenter.attachView(view)
  }

  @Test
  fun load_withRepositoryHavingEmptyFavoriteRecipes_callsShowEmptyRecipes() {
    whenever(repository.getFavoriteRecipes()).thenReturn(emptyList())

    presenter.loadFavoriteRecipes()

    verify(view).showEmptyRecipes()
  }

  @Test
  fun load_withRepositoryHavingFavoriteRecipes_callsShowRecipes() {
    val recipe = Recipe("id", "title", "imageUrl", "sourceUrl", true)
    val favorites = listOf(recipe)
    whenever(repository.getFavoriteRecipes()).thenReturn(favorites)

    presenter.loadFavoriteRecipes()

    verify(view).showRecipes(eq(favorites))
  }
}